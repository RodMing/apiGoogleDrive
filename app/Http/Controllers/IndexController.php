<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{

    const TOKEN='token';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function post(Request $request)
    {
        if ($request->has(self::TOKEN)) {
            return $this->getUrlAutenticate($request->input(self::TOKEN));
        }

        return response()->json($request->all());
    }

    private function getUrlAutenticate($token)
    {
        return response()->json($token);
    }
}
